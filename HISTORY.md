April 27, 2018
 - No updates

May 4, 2018
 - Installed PyEnchant
 - Can process words in loops
 - Can check for all possible letter combinations
 - Can segregate combinations with meaning
 - can segregate combinations by letter count

May 11, 2018
 - No Updates

May 18, 2018
 - No Updates

May 25, 2018
 - No Updates
